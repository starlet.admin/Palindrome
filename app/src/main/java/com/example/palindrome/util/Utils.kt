package com.example.palindrome.util

object Utils {

    fun isPalindrome(text: String?): Boolean {
        if (text == null || text.trim { it <= ' ' }.isEmpty()) {
            return false
        } else {
            return StringBuilder(text).reverse().toString().toLowerCase() == text.toLowerCase()
        }
    }
}