package com.example.palindrome.presentation.ui.activity

import android.os.Bundle
import android.widget.Toast
import com.example.palindrome.R
import com.example.palindrome.presentation.presenter.MainPresenter
import com.example.palindrome.presentation.ui.view.MainView
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpActivity<MainView, MainPresenter>(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        palindromeCheckButton.setOnClickListener {
            presenter.checkPalindrome(palindromeEditText.text.toString())
        }
    }

    override fun createPresenter(): MainPresenter {
        return MainPresenter()
    }

    override fun onPalindromeCheckResult(text: String, palindrome: Boolean) {
        Toast.makeText(this,
                if (palindrome)
                    String.format(getString(R.string.this_is_palindrome), text)
                else
                    String.format(getString(R.string.this_is_not_palindrome), text),

                Toast.LENGTH_LONG)
                .show()
    }
}