package com.example.palindrome.presentation.ui.view

import com.hannesdorfmann.mosby3.mvp.MvpView

interface MainView : MvpView {

    fun onPalindromeCheckResult(text: String, palindrome: Boolean)
}