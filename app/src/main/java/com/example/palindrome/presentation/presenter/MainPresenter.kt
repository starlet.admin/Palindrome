package com.example.palindrome.presentation.presenter

import com.example.palindrome.presentation.ui.view.MainView
import com.example.palindrome.util.Utils
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

class MainPresenter : MvpBasePresenter<MainView>() {

    fun checkPalindrome(text: String) {
        val palindrome = Utils.isPalindrome(text)
        view.onPalindromeCheckResult(text, palindrome)
    }
}